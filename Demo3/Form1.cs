﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Demo3
{
    public partial class Form1 : Form
    {
        private const double BASE_CHARGE = 13.50;
        private const double TOPPING_CHARGE = 1.00;
        private const double DELIVERY_CHARGE = 2.00;
        
        private int _toppings = 0;
        private bool _delivery = false;
        private double _price = 0;

        public Form1()
        {
            InitializeComponent();
            //UpdatePrice();
            outputLabel.Text = "";
        }

        private void UpdatePrice()
        {
            _price = BASE_CHARGE + TOPPING_CHARGE * _toppings;
            if (_delivery)
            {
                _price += DELIVERY_CHARGE;
            }
            outputLabel.Text = string.Format(
                "Total Price is {0:C}",
                _price
            );
        }

        private void pickupRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            _delivery = false;
            UpdatePrice();
        }

        private void deliverRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            _delivery = true; //deliverRadioButton.Checked;
            UpdatePrice();
        }

        private void dineinRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            _delivery = false;
            UpdatePrice();
        }

        private void HandleToppingCheckBox(object sender)
        {
            bool chk = ((CheckBox)sender).Checked;
            if (chk)
            {
                ++_toppings;
            }
            else
            {
                --_toppings;
            }
            UpdatePrice();
        }

        private void onionsCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            HandleToppingCheckBox(sender);
        }

        private void greenPepperCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            HandleToppingCheckBox(sender);
        }

        private void pepperoniCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            HandleToppingCheckBox(sender);
        }

        private void sausageCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            HandleToppingCheckBox(sender);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var about = new AboutForm();
            about.ShowDialog();
        }
    }
}
