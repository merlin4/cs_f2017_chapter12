﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Demo4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            outputLabel.Text = "";

            listBox1.Items.Clear();
            listBox1.Items.AddRange(
                new object[]
                {
                    new Student { Name = "Fred", GPA = 3.5, Age = 60 },
                    new Student { Name = "Wilma", GPA = 3.6, Age = 58 }
                }
            );
        }

        private void outputLabel_Click(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string output = "";
            foreach (Student student in listBox1.SelectedItems)
            {
                output += string.Format(
                    "Name {0}, GPA {1}, Age {2}\n",
                    student.Name, student.GPA, student.Age
                );
            }
            outputLabel.Text = output;

            //Student student = (Student)listBox1.SelectedItem;
            //outputLabel.Text = string.Format(
            //    "Name {0}, GPA {1}, Age {2}",
            //    student.Name, student.GPA, student.Age
            //);
        }
    }
}
