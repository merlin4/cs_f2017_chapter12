﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Demo5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            messageLabel.Text = "";
        }

        private void calendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            DateTime now = DateTime.Now;
            DateTime start = e.Start;
            DateTime tenDays = start.AddDays(10);

            messageLabel.Text = string.Format(
                "Date {0} days after selection is {1:d}",
                10, tenDays
            );
        }
    }
}
