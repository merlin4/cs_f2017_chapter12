﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Demo1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            var specialFont = new Font("Courier New", 16.5f);
            label1.Font = specialFont;
            okButton.Font = specialFont;

            this.BackColor = Color.Purple;
            label1.ForeColor = Color.FromArgb(0, 255, 0);
        }
    }
}
